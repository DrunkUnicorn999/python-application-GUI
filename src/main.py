from main_window import Ui_MainWindow as main_window
import sys

from PyQt5.QtWidgets import QMainWindow, QApplication


class Calculate:
    def eqn_1(self, x):
        return (x ** 2 + 5) / 6
    
    def eqn_2(self, x):
        return (6 * x - 5) ** 0.5
    
    def calc(self, method, x0, n):
        x = x0
        for _ in range(1, n + 1):
            x = method(x)
        return x
        

class MainWindow(QMainWindow, main_window):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Решение уравнений методом последовательных приближений')
        self.label.setText('<html><head/><body><p align="center">Нажмите кнопку "Запустите программу"</body></html>')
        self.calc = Calculate()

        # ---- Handlers ----
        self.pushButton.clicked.connect(self.execute)
        
    def execute(self):
        current = self.comboBox.currentIndex()
        if current == 0:
            self.label.setText(f'<html><head/><body><p align="center"><b>Ответ: </b>(x ** 2 + 5) / 6 при (0, 10) -> {self.calc.calc(self.calc.eqn_1, 0, 10)}</p></body></html>')
        elif current == 1:
            self.label.setText(f'<html><head/><body><p align="center"><b>Ответ: </b>(6 * x - 5) ** 0.5 при (4, 10) -> {self.calc.calc(self.calc.eqn_2, 4, 10)}</p></body></html>')
            

def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
